let myTask = [
	'bake css',
	'drink HTML',
	'inhale css',
	'eat js',
];


// let lastIndex = myTask.length - 1;
// console.log(myTask[lastIndex])
console.log(myTask);
console.log(myTask.length)

// PUSH METHOD
let myTaskLen = myTask.push('Feed Dogs', 'Feed Cat', 'Slaughter Cows');
console.log(myTask);
console.log(myTaskLen)

// POP METHOD
let removedTask = myTask.pop();
console.log(myTask)
console.log(removedTask)

// SHIFT METHOD
let shiftElement = myTask.shift();
console.log(myTask)
console.log(shiftElement)

// UNSHIFT METHOD
let unShiftElement = myTask.unshift('Fry Fish', 'Throw Birds');
console.log(myTask)
console.log(unShiftElement)

// SPLICE METHOD - arrayName.splice(starting index,delete count, eletments to be added)
let spliceMethod = myTask.splice(2,3,'NewTask1', 'NewTask2', 'NewTask3' );
console.log("splice returned: " + spliceMethod);
console.log(myTask)

// SORT METHOD - A to Z
myTask.sort();
console.log(myTask)

// REVERSE METHOD - Z to A
myTask.reverse();
console.log(myTask)

console.log("*************************************************");
//------------------------ NON MUTATOR ----------------------------------------

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
console.log("Countries Array " + countries );
// indexOF method
console.log("indexOf PH " + countries.indexOf('PH'));
// Missing array entry
console.log("index of RS " + countries.indexOf('RS'));
//lastIndexOf() - returns the last matching element found in array
// syntax: arrayName.lastIndexOF(searchValue)
let lastIndex = countries.lastIndexOf('PH');
console.log("Result of Last Index of PH " + lastIndex );

// SLICE METHOD
let slicedArrayA = countries.slice(2);
let slicedArrayB = countries.slice(2,4);
console.log("SliceArrayA " + slicedArrayA )
console.log("SliceArrayB " + slicedArrayB )

//TO STRING
let stringArray = countries.toString();
console.log("Result of toString() " + stringArray )

//concat - combines two or more arrays and return the combined result	
let tasksArrayA = ['drink HTML', 'eat Javascript'];
let tasksArrayB = ['inhales CSS', 'breath sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat (tasksArrayB);
console.log ('Result from concat()');
console.log(tasks);
let allTasks = tasksArrayA.concat (tasksArrayB, tasksArrayC);
console.log ('Result from concat()');
console.log(allTasks);

let combinedTasks = tasksArrayA.concat ('smell express', 'throw react');
console.log ('Result from concat()');
console.log(combinedTasks);


//join - returns an array as string separated by specified separator
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));
console.log(users.join(' & '));	


//------------------------ ITERATION METHOD ----------------------------------------
console.log(allTasks)

// FOREACH  - looping per entry of an Array
//forEaCH - conventional
allTasks.forEach(function(task){
	console.log(task);
})	

//anonymos function
allTasks.forEach(task => console.log(task)
);

let filterTask = [];

allTasks.forEach(task => {
	if(task.length > 10){
		filterTask.push(task);
	}
	}
);

console.log("Filtered Task " + filterTask )

// MAP  - RETURNED NEW ARRAY AND REQUIRES NEW ARRAY

// let numbers = [1,2,3,4,5];

// let numberMap = numbers.map(function(number){
// 	return number * number;
// })

// console.log("Result from Map " + numberMap);	

let numbers = [1,2,3,4,5];

// let numberMap = numbers.map(function(number){
// 	//return number + 5;
// 	console.log("Result of numberMap " + number + 3)
// 	return number + 3;
// })

let numberMap = numbers.map(number => {
	return number * number;
})

console.log("Result from Map " + numberMap);	

// EVERY
console.log("numbers array: " + numbers);

let allValid = numbers.every(function(number){
	return (number < 3);
})

console.log("EVERY" + allValid);

//SOME

let allSomeValid = numbers.some(function(number){
	return (number < 3);
})

console.log("Some: " + allSomeValid);


//FILTER

let filterValid = numbers.filter(function(number){
	return (number < 3);
})

console.log("Result from Filter: " + filterValid );

//INCLUDES

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

// let filterProducts = products.filter(function(prod){
// 	return products.toLowerCase().includes('a');
// })
// console.log("Result from Filter() and Includes" + filterProducts );

//REDUCES

//Multi Dimentional

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log('Pawn moves to f2: ' + chessBoard)